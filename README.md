# noroff-assignment-003-web-server-development-in-asp.net-core 

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A solution for the acadmy assignment 003, web server development in asp.net core 

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Install

```
Clone repository and open solution in Visual Studio  
Edit the "Data Source" part of the connection string in MovieDbContext.cs Line 18, located in Models/Data, to point to your local SQL Server instance
Make sure no database named MovieDb exists on your system or change the name of the database to be created in the connection string in MovieDbContext.cs Line 18, located in Models/Data
Open the Package Manager Console in Visual Studio an run the following commands:   
	  
	add-migration initial-migration  
	update-database  
  
```

## Usage

```
Start the IIS Express from Visual Studio and use the provided Swagger ui to test the api
  		
```

## Maintainers

	Daniel Ruegenberg, Rick Schnellert


## License

	MIT © 2022 Daniel Ruegenberg

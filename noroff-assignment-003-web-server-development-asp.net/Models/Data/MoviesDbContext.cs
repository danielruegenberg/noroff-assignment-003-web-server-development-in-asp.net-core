﻿using Microsoft.EntityFrameworkCore;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain;

namespace noroff_assignment_003_web_server_development_asp.net.Models.Data
{
    public class MoviesDbContext : DbContext
    {
        public MoviesDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source = DESKTOP-25BRQ4V\SQLEXPRESS; initial Catalog = MovieDb; Integrated Security = true; Trust Server Certificate = True");
        }
        /// <summary>
        /// Populating database with default values
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>().HasData(
                new Character() { Id = 1, FullName = @"Harry Potter", Alias = @"The boy that survived", Gender = @"Male", Picture = @"https://m.media-amazon.com/images/M/MV5BMTkyMDM1NDA5MV5BMl5BanBnXkFtZTgwMzE0ODAxMTI@._V1_FMjpg_UX1000_.jpg"},
                new Character() { Id = 2, FullName = @"Hermione Granger", Alias = @"", Gender = @"Female", Picture = @"https://m.media-amazon.com/images/M/MV5BNmExMjY1ZTctZTRhZS00NjljLTg3NTctODRhZDEzMTU0MTdlXkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_FMjpg_UX1000_.jpg" },
                new Character() { Id = 3, FullName = @"Katniss Everdeen", Alias = @"Mockingjay", Gender = @"Female", Picture = @"https://m.media-amazon.com/images/M/MV5BMTY4NTIzNDA3NF5BMl5BanBnXkFtZTgwNjIzNTYyNzE@._V1_FMjpg_UX1000_.jpg" }
                );
            modelBuilder.Entity<Franchise>().HasData(
                new Franchise() { Id = 1, Name = @"Harry Potter", Description = @"Story of a young boy and his friends, becoming adult in a world full of magic adventures." },
                new Franchise() { Id = 2, Name = @"Hunger Games", Description = @"A young woman tries to survive a brutal TV show and conquers the authorities in an absolute regime. " }
                );
            modelBuilder.Entity<Movie>().HasData(
                new Movie() { Id = 1, MovieTitle = @"Harry Potter and the sorcerer's stone", Genre = @"Fantasy", ReleaseYear = 2001, Director = @"Chris Columbus", FranchiseId = 1, Picture = @"https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_FMjpg_UX1000_.jpg", Trailer = @"https://www.youtube.com/watch?v=VyHV0BRtdxo" },
                new Movie() { Id = 2, MovieTitle = @"Harry Potter and the chamber of secrets", Genre = @"Fantasy", ReleaseYear = 2002, Director = @"Chris Columbus", FranchiseId = 1, Picture = @"https://m.media-amazon.com/images/M/MV5BMTcxODgwMDkxNV5BMl5BanBnXkFtZTYwMDk2MDg3._V1_.jpg", Trailer = @"https://www.youtube.com/watch?v=VyHV0BRtdxo" },
                new Movie() { Id = 3, MovieTitle = @"The Hunger Games", Genre = @"Science Fiction", ReleaseYear = 2012, Director = @"Gary Ross", FranchiseId = 2, Picture = @"https://m.media-amazon.com/images/M/MV5BMjA4NDg3NzYxMF5BMl5BanBnXkFtZTcwNTgyNzkyNw@@._V1_FMjpg_UX1000_.jpg", Trailer = @"https://www.youtube.com/watch?v=mfmrPu43DF8" }
                );
            modelBuilder.Entity("CharacterMovie").HasData(
            new { CharactersInMovieId = 1, MoviesWithCharacterId = 1 },
            new { CharactersInMovieId = 1, MoviesWithCharacterId = 2 },
            new { CharactersInMovieId = 2, MoviesWithCharacterId = 1 },
            new { CharactersInMovieId = 2, MoviesWithCharacterId = 2 },
            new { CharactersInMovieId = 3, MoviesWithCharacterId = 3 }
            );
        }
    }
}

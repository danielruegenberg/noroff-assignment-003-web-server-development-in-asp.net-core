﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace noroff_assignment_003_web_server_development_asp.net.Models.Domain
{
    public class Movie
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string MovieTitle{ get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(70)]
        public string Director { get; set; }
        [MaxLength(400)]
        public string Picture { get; set; }
        [MaxLength(400)]
        public string Trailer { get; set; }
        public int? FranchiseId { get; set; }
        public ICollection<Character> CharactersInMovie{ get; set; }
    }
}

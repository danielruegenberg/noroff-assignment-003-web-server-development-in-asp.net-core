﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace noroff_assignment_003_web_server_development_asp.net.Models.Domain
{
    public class Franchise
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(150)]
        public string Description { get; set; }
        public ICollection<Movie> FranchiseMovies { get; set; }
    }
}

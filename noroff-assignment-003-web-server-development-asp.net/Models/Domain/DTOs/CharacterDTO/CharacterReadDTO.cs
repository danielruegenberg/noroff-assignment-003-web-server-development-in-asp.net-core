﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace noroff_assignment_003_web_server_development_asp.net.Models.Domain.DTOs.CharacterDTO
{
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        [MaxLength(70)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(10)]
        public string Gender { get; set; }
        [MaxLength(400)]
        public string Picture { get; set; }
        public int[] Movies { get; set; }
    }
}

﻿using AutoMapper;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain.DTOs.FranchiseDTO;
using System.Linq;

namespace noroff_assignment_003_web_server_development_asp.net.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ReverseMap();
            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();
            CreateMap<Franchise, FranchiseUpdateDTO>()
                .ReverseMap();
        }
    }
}

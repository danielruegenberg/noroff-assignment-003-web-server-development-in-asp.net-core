﻿using AutoMapper;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain.DTOs.MovieDTO;
using System.Linq;

namespace noroff_assignment_003_web_server_development_asp.net.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieCreateDTO>()
                .ReverseMap();
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(pdto => pdto.Characters, opt =>
                opt.MapFrom(p => p.CharactersInMovie.Select(s => s.Id).ToArray()))
                .ReverseMap();
            CreateMap<Movie, MovieUpdateDTO>()
                .ReverseMap();
        }
    }
}

﻿using AutoMapper;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain.DTOs.CharacterDTO;
using System.Linq;

namespace noroff_assignment_003_web_server_development_asp.net.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            //CreateMap<Character, CharacterReadDTO>()
            //    .ForMember(pdto => pdto.MoviesWithCharacter, opt => opt.MapFrom(p => p.MoviesWithCharacter.Select(s => s.Id).ToArray()))
            //    .ReverseMap();
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt.MapFrom(c => c.MoviesWithCharacter.Select(m => m.Id).ToArray()))
                .ReverseMap();
            CreateMap<Character, CharacterCreateDTO>()
                .ReverseMap();
            CreateMap<Character, CharacterUpdateDTO>()
                .ReverseMap();
        }
    }
}

﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using noroff_assignment_003_web_server_development_asp.net.Models.Data;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain.DTOs.CharacterDTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace noroff_assignment_003_web_server_development_asp.net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharacterController : ControllerBase
    {
        private MoviesDbContext _context;
        private IMapper _mapper;
        public CharacterController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper= mapper;
        }
        /// <summary>
        /// Gets a list of characters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacter()
        {
            List<Character> characterList = new List<Character>();
            List<CharacterReadDTO> characterListToSend;
            characterList = await _context.Characters.Include(c => c.MoviesWithCharacter).ToListAsync();
            if (characterList == null)
            {
                return NotFound();
            }
            characterListToSend = _mapper.Map<List<CharacterReadDTO>>(characterList);
            return Ok(characterListToSend);
        }
        /// <summary>
        /// Gets a character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int id)
        {
            Character singleCharacter = new Character();
            singleCharacter = await _context.Characters.Include(c => c.MoviesWithCharacter).FirstOrDefaultAsync(c => c.Id == id);
            if (singleCharacter == null)
            {
                return NotFound();
            }
            CharacterReadDTO singleCharacterToSend = _mapper.Map<CharacterReadDTO>(singleCharacter);
            return Ok(singleCharacterToSend);
        }
        /// <summary>
        /// Creates a character
        /// </summary>
        /// <param name="newCharacter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CharacterCreateDTO>> PostCharacter([FromBody] CharacterCreateDTO newCharacter)
        {
            var domainCharacter = _mapper.Map<Character>(newCharacter);
            _context.Characters.Add(domainCharacter);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetCharacterById", new {id = domainCharacter.Id}, newCharacter);
        }
        /// <summary>
        /// Deletes a character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(404)]
        [ProducesResponseType(204)]        
        public async Task<ActionResult> DeleteCharacter(int id)
        {
            var characterToDelete = await _context.Characters.FindAsync(id);
            if(characterToDelete == null)
            {
                return NotFound();
            }
            _context.Characters.Remove(characterToDelete);
            await _context.SaveChangesAsync();
            return NoContent();
        }
        /// <summary>
        /// Gets a character by id and overwrites it with an updated version
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newCharacter"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(204)]
        public async Task<ActionResult> UpdateCharacter(int id, [FromBody]CharacterUpdateDTO newCharacter)
        {
            if(id != newCharacter.Id)
            {
                return BadRequest();
            }
            var domainCharacter = _mapper.Map<Character>(newCharacter);
            _context.Entry(domainCharacter).State = EntityState.Modified;
            await _context.SaveChangesAsync();           
            return NoContent();
        }
        /// <summary>
        /// Adds a movie to a character by populating the linking table CharacterMovie with both primary keys
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieId"></param>
        /// <returns></returns>
        [HttpPut("{id}/assignmovietocharacter")]
        public async Task<ActionResult> AssignMovieToCharacter(int id, int movieId)
        {
            var characterForMovieAssignment = await _context.Characters.FindAsync(id);
            if (characterForMovieAssignment == null)
            {
                return NotFound();
            }
            characterForMovieAssignment = await _context.Characters
                .Include(c => c.MoviesWithCharacter)
                .Where(c => c.Id == id)
                .FirstAsync();
            Movie singleMovie = new Movie();
            singleMovie = await _context.Movies.FindAsync(movieId);
            if (singleMovie == null)
            {
                return BadRequest("Movie does not exist");
            }
            characterForMovieAssignment.MoviesWithCharacter.Add(singleMovie);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return NoContent();
        }
    }
}

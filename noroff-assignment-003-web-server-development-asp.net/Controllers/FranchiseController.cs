﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using noroff_assignment_003_web_server_development_asp.net.Models.Data;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain.DTOs.FranchiseDTO;

namespace noroff_assignment_003_web_server_development_asp.net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        private MoviesDbContext _context;
        private IMapper _mapper;

        public FranchiseController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a list of franchises
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchise()
        {
            var franchiseListToSend = _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises.ToListAsync<Franchise>());
            if (franchiseListToSend == null)
            {
                return NotFound();
            }
            return Ok(franchiseListToSend);
        }
        /// <summary>
        /// Gets a franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            FranchiseReadDTO franchiseToSend = _mapper.Map<FranchiseReadDTO>(franchise);
            return Ok(franchiseToSend);
        }
        /// <summary>
        /// Creates a new franchise
        /// </summary>
        /// <param name="newFranchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> PostFranchise([FromBody] FranchiseCreateDTO newFranchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(newFranchise);
            
            _context.Franchises.Add(domainFranchise);
            
            await _context.SaveChangesAsync();
            
            return CreatedAtAction("GetFranchiseById", new {id = domainFranchise.Id}, newFranchise);
        }
        /// <summary>
        /// Deletes a franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(404)]
        [ProducesResponseType(204)]
        public async Task<ActionResult> DeleteFranchise(int id)
        {
            var franchiseToDelete = await _context.Franchises.FindAsync(id);
            if (franchiseToDelete == null)
            {
                return NotFound();
            }
            _context.Franchises.Remove(franchiseToDelete);
            await _context.SaveChangesAsync();
            return NoContent();
        }
        /// <summary>
        /// Gets a franchise by id and overwrites it with an updated version
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newFranchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(204)]
        public async Task<ActionResult>UpdateFranchise(int id, [FromBody]FranchiseUpdateDTO newFranchise)
        {
            if (id != newFranchise.Id)
            {
                return BadRequest();
            }
            var domainFranchise = _mapper.Map<Franchise>(newFranchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}

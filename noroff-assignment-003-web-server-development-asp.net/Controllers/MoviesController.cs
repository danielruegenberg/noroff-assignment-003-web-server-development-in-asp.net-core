﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using noroff_assignment_003_web_server_development_asp.net.Models.Data;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain;
using noroff_assignment_003_web_server_development_asp.net.Models.Domain.DTOs.MovieDTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace noroff_assignment_003_web_server_development_asp.net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private MoviesDbContext _context;
        private IMapper _mapper;

        public MovieController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Gets a list of movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovie()
        {
            List<Movie> movieList = new List<Movie>();
            List<MovieReadDTO> movieListToSend = new List<MovieReadDTO>();
            movieList = await _context.Movies.Include(m => m.CharactersInMovie).ToListAsync();
            if (movieListToSend == null)
            {
                return NotFound();
            }
            movieListToSend = _mapper.Map <List<MovieReadDTO>>(movieList);
            return Ok(movieListToSend);
        }
        /// <summary>
        /// Gets a movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<MovieReadDTO>> GetMovieById(int id)
        {
            Movie singleMovie = new Movie();
            singleMovie = await _context.Movies.Include(m => m.CharactersInMovie).FirstOrDefaultAsync(m => m.Id == id);
            if (singleMovie == null)
            {
                return NotFound();
            }
            MovieReadDTO movieToSend = _mapper.Map<MovieReadDTO>(singleMovie);
            return Ok(movieToSend);
        }
        /// <summary>
        /// Creates a new moview
        /// </summary>
        /// <param name="newMovie"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostMovie([FromBody] MovieCreateDTO newMovie)
        {
            var domainMovie= _mapper.Map<Movie>(newMovie);

            _context.Movies.Add(domainMovie);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovieById", new { id = domainMovie.Id }, newMovie);
        }
        /// <summary>
        /// Deletes a movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(404)]
        [ProducesResponseType(204)]
        public async Task<ActionResult> DeleteMovie(int id)
        {
            var movieToDelete = await _context.Movies.FindAsync(id);
            if (movieToDelete == null)
            {
                return NotFound();
            }
            _context.Movies.Remove(movieToDelete);
            await _context.SaveChangesAsync();
            return NoContent();
        }
        /// <summary>
        /// Gets a movie by id and overwrites it with an updated version
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newMovie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(204)]
        public async Task<ActionResult> UpdateMovie(int id, [FromBody]MovieUpdateDTO newMovie)
        {
            if (id != newMovie.Id)
            {
                return BadRequest();
            }
            var domainMovie = _mapper.Map<Movie>(newMovie);
            _context.Entry(domainMovie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}
